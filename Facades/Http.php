<?php

namespace JD\Http\Facades;

use October\Rain\Support\Facade;

class Http extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'jd.http';
    }
}
