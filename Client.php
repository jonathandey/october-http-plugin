<?php

namespace JD\Http;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

class Client
{
    /**
     * @var HandlerStack $handlerStack
     */
    private $handlerStack;

    /**
     * @var $handler
     */
    private $handler = null;

    /**
     * @var boolean
     */
    private $faking = false;

    /**
     * @var Response $response
     */
    private $response;

    /**
     * @var boolean $shouldRetry
     */
    private $shouldRetry = false;

    /**
     * @var int $maxRetries
     */
    private $maxRetries;

    /**
     * @var float $retryTimeout
     */
    private $retryTimeout;

    public function __construct()
    {
        $this->handlerStack = $this->createHandlerStack();

        $this->resetHttpClient();
    }

    public function fake(array $fakes = [])
    {
        $this->faking = true;

        // Set a 200 empty response by default
        if (empty($fakes)) {
            $fakes = [
                new Response(200, [], ''),
            ];
        }

        $this->handler = new MockHandler($fakes);

        $this->handlerStack = $this->createHandlerStack($this->handler);
        $this->resetHttpClient();
    }

    public function retry($retries, $retryTimeout = 1)
    {
        $this->shouldRetry = true;

        $this->maxRetries = $retries;

        $this->retryTimeout = $retryTimeout;

        return $this;
    }

    public function request()
    {
        $args = func_get_args();

        if (! $this->shouldRetry) {
            $this->response = $this->http->request(...$args);
        } else {
            $this->response = retry($this->maxRetries, function() use ($args) {
                return $this->http->request(...$args);
            }, $this->retryTimeout);
        }

        // Rewind the responses to be used again
        if ($this->faking) {
            $this->handler->reset();
        }

        return $this->response->getBody()->getContents();
    }

    public function getResponse()
    {
        return $this->response;
    }

    private function createHandlerStack()
    {
        return HandlerStack::create($this->handler);
    }

    private function resetHttpClient()
    {
        $this->http = new GuzzleHttpClient(
            [
                "handler" => $this->handlerStack
            ]
        );
    }
}
